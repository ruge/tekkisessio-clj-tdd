(ns clj-tdd.deps-test
  (:require [clojure.test :refer :all]
            [clj-tdd.deps :as deps]))


(deftest hours-to-report-test
  (testing "concatenate report string"
    (is (= "Week: 45 [5.7 6.5]" (deps/hours-to-report 45 [5.7 6.5])))))
