(ns clj-tdd.problem-test
  (:require [clojure.test :refer :all]
            [clj-tdd.core :refer :all]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]))

(def calls (atom {}))

(defn store-args! [key args]
  (swap! calls assoc key args))

(defn get-args [keys]
  (get-in @calls keys))

(def expected-hours (gen/sample (s/gen decimal?)))
(def report-string (gen/sample (s/gen string?)))
(def address (gen/sample (s/gen string?)))

(defn workweek-hours [week-number]
  (store-args! :workweek-hours {:week-number week-number})
  expected-hours)

(defn hours-to-report [week-number hours]
  (store-args! :hours-to-report {:week-number week-number
                                 :hours       hours})

  ;; Asserts can be inside mocks which removes need for calls state. Though this hinders test readability
  (is (= hours expected-hours))
  report-string)

(defn email-report [address report]
  (store-args! :email-report {:address address
                              :report  report}))

(deftest problem-test
  (let [week-number (gen/generate (s/gen integer?))
        actual (weekly-report workweek-hours hours-to-report email-report week-number address)]

    (testing "get workweek form harves api"
      (is (= week-number (get-args [:workweek-hours :week-number]))))

    (testing "create report"
      (is (= week-number (get-args [:hours-to-report :week-number])))
      (is (= expected-hours (get-args [:hours-to-report :hours]))))

    (testing "send report email"
      (is (= address (get-args [:email-report :address])))
      (is (= report-string (get-args [:email-report :report]))))))
