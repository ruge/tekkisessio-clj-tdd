# How to test drive algorithms with top-down design in clojure?

TLDR; This process works well with mutable state, not so well without it.

### Example: Create weekly report from harvest and send it to client.

Algorithm in pseudo code

1. Get workweek hours from harvest api
1. Create report from hours for given week
1. Send report to client email

### Top-down design process with TDD

1. Start from highest abstraction level
1. Test drive main algorithm first
1. Mock/Stub all low level functions
1. After main algorithm is finished start implementing lower level functions
1. Repeat until all abstraction tiers are tested

### Benefits
This kind of process automatically introduces abstractions and seams into code.
Usually easier to start from top than from low level implementation details.

Follows OSP (Open-Closed Principle) and ISP (Interface Seggregation Principle) automatically.

OSP for seams and ISP because client api is written before implementation.

## Problem:
Testing that dependencies are called with correct parameters requires mutable state.
In test we need to verify that dependent function was called with correct parameters.
This is usually done by storing call parameters and verified after function call in end of test.

### Solutions 1: Function composition
Use function composition and tests all low level functions independently.

* Testing top level composition is usually ignored
* Requires that functions are easily composeable eg. "pipeable"

### Solution 2: Ignoring testing side-effect functions
Like solution 1. but we ignore testing functions with side-effects.

eg. We do not test that side effect function is called with correct parameters.
This can be acceptable in static typed languages like haskell, but not in clojure where compiler does not check call parameters.

### Solution 3: Use mocks/stubs
Introduce mutable state or use some mocking library like Midje.

### Solution 4: Monads (Reader/Writer/Either) for abstracting side-effect
Haskell resolves this problem by using monads, solution also includes function composition.
Problem here is that monads are not native in clojure so they are not commonly used.

### Solution 5: Other?
