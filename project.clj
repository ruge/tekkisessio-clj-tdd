(defproject clj-tdd "0.1.0-SNAPSHOT"
  :description "TDD Top-down demo"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/test.check "0.10.0-alpha2"]]
  :main ^:skip-aot clj-tdd.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
