(ns clj-tdd.core
  (:gen-class)
  (:require [clj-tdd.deps :as deps]))

;; without composition
(defn weekly-report [workweek-hours hours-to-report email-report week-number address]
  (->> (workweek-hours week-number)
       (hours-to-report week-number)
       (email-report address)))

;; with composition
(defn weekly-report2 [workweek-hours hours-to-report email-report]
  (-> (workweek-hours)
      (hours-to-report)
      (email-report)))


(defn -main
  [& args]
  (let [week-number (first args)
        address (second args)
        weekly-report-impl (weekly-report2
                             (partial deps/workweek-hours week-number)
                             ;; We can inject dependencies to partially applied functions
                             (partial deps/hours-to-report2 "Jejeee" week-number)
                             (partial deps/email-report address))]

    weekly-report-impl))
