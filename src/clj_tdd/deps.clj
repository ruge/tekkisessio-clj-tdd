(ns clj-tdd.deps)

(defn workweek-hours [week-number]
  [7.5 6.5 9 7 6.5])

(defn hours-to-report [week-number hours]
  (str "Week: " week-number " " hours))

(defn hours-to-report2 [dependency week-number hours]
  (str "Special repor" dependency))


(defn email-report [address report]
  (println (str "Send email to " address " with report: " report)))
